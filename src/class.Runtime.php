<?php
/**
 * @file class.Runtime.php
 * @brief 页面运行时操作
 * 例子
 * $runtime= new Runtime();
 * echo $runtime->spend();
 * 
 */
class Runtime {
	private $_startTime = 0;
	private $_stopTime  = 0;

	public function __construct() {
		$this->start();
	}

	//毫秒级的时间戳
	private function getMicrotime() {
		list($usec, $sec) = explode(' ', microtime());
		return ((float)$usec + (float)$sec);
	}

	//起始
	private function start() {
		$this->_startTime = $this->getMicrotime();
	}

	//结束
	private function stop() {
		$this->_stopTime = $this->getMicrotime();
	}

	//耗时
	public function spend() {
		$this->stop();
		return round(($this->_startTime - $this->_stopTime) * 1000, 2);
	}

	public function __destruct() {
		unset($this->_startTime, $this->_stopTime);
	}
}
